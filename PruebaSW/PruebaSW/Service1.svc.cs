﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace PruebaSW {
	public class Service1 : IService1 {

		public double realizarCalculo(int tipo, int a, int b) {
			return tipo == 1 ? (a + b) : tipo == 2 ? (a - b) : tipo == 3 ? (a * b) : tipo == 4 ? (a / b) :
				tipo == 5 ? Math.Pow(a + b, 2) : tipo == 6 ? Math.Sin(a + b) : tipo == 7 ?
				Math.Cos(a + b) : Math.Tan(a + b);
		}
	}
}
